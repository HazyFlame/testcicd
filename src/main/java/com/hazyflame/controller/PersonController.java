package com.hazyflame.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hazyflame.model.Person;

@RestController
public class PersonController {

//	Url
//		http://localhost:7977/getPersonById?id=1
//	Method
//		GET
	@RequestMapping(value = "/getPersonById", method = RequestMethod.GET)
	public ResponseEntity<?> getPersonById(String id) {
		Person per = null;
		try {
			if ("1".equals(id.toString())) {
				per = new Person(1, "Bùi Ngọc Duy", 26, "163233007");
			} else {
				per = new Person();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<Person>(per, HttpStatus.OK);
	}

//	Url 
//		http://localhost:7977/getPerson
//	Method
//		POST
//	Body
//		{
//			"id":1,
//			"name":"",
//			"age":26,
//			"idCard":""
//		}
	@RequestMapping(value = "/getPerson", method = RequestMethod.POST)
	public ResponseEntity<?> getPerson(@RequestBody Person person) {
		Person per = null;
		try {
			per = new Person(1, "Bùi Ngọc Duy", 26, "163233007");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<Person>(per, HttpStatus.OK);
	}
}