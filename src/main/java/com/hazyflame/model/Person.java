package com.hazyflame.model;

public class Person {
	private int id;
	private String name;
	private int age;
	private String idCard;

	public Person() {
		super();
	}

	public Person(int id, String name, int age, String idCard) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.idCard = idCard;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

}
