package com.hazyflame.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@SpringBootApplication
@ComponentScan({ "com.hazyflame" })
public class ApplicationMain {

	public static void main(String[] args) {
		try {
			SpringApplication.run(ApplicationMain.class, args);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
