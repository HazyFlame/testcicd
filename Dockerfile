FROM java:8
COPY target/myRESTful-0.0.1-SNAPSHOT.jar docker_jenkin.jar
EXPOSE 7977
ENTRYPOINT ["java","-jar","/docker_jenkin.jar"]